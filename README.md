# Overvoltage and Reverse Polarity Protection circuit using Jelly Bean parts

![](https://img.shields.io/badge/V7-KiCad-blue)
Simulation is working with KiCAD 7 and not tested with the older versions

![schematic](images/input_protection.jpg)
Rest of the description to be added later. 
Library to be updated to match the schematic

![simulation_output](images/ResultingPlot.png)

Keyword: LM431 / TL431

## License
[Attribution 4.0 International (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/)